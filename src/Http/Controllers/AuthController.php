<?php

namespace PassTicketTools\Http\Controllers;

use App\Http\Controllers\Controller;
use App\User;
use PassTicketTools\Library\PassportClient;

/**
 * Class AuthController.
 */
class AuthController extends Controller
{
    protected function redirect()
    {
        return redirect()->to(config('services.passticket_tools.login_redirect'));
    }

    /**
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function login()
    {
        $passport = new PassportClient();

        if ($passport->needAuth()) {
            return redirect($passport->getAuthUrl());
        }

        $user = User::where('passport_tools_user_id', $passport->getUser()->id)
            ->orWhere('email', $passport->getUser()->email)
            ->first();

        if (!$user) {
            $user = new User();
        }

        $user->name = $passport->getUser()->name;
        $user->email = $passport->getUser()->email;
        $user->passport_tools_user_id = $passport->getUser()->id;

        $user->save();

        auth()->login($user, true);

        return $this->redirect();
    }
}
