<?php

namespace PassTicketTools\Library;

use GuzzleHttp\Client;

/**
 * Class PassportClient
 * @package PassTicketTools\Library
 */
class PassportClient
{

    /**
     * @var Client
     */
    protected $http;

    /**
     * @var \Illuminate\Config\Repository|mixed
     */
    protected $clientId;
    /**
     * @var \Illuminate\Config\Repository|mixed
     */
    protected $secret;
    /**
     * @var \Illuminate\Config\Repository|mixed
     */
    protected $baseUrl;
    /**
     * @var \Illuminate\Config\Repository|mixed
     */
    protected $redirect;
    /**
     * @var \Illuminate\Config\Repository|mixed
     */
    protected $scope;

    /**
     * @var string
     */
    protected $sessionKey = 'passticket_access_token';

    /**
     * @var
     */
    protected $user;

    /**
     * PassportClient constructor.
     */
    public function __construct()
    {
        $this->clientId = config('services.passticket_tools.id');
        $this->secret = config('services.passticket_tools.secret');
        $this->baseUrl = config('services.passticket_tools.server');
        $this->redirect = config('services.passticket_tools.redirect');
        $this->scope = config('services.passticket_tools.scope');

        $this->http = new Client($this->getOptions());

        $this->getAccessToken();
    }

    /**
     * @return bool
     */
    public function needAuth()
    {
        return !$this->existCode();
    }

    /**
     * @return array|\Illuminate\Http\Request|string
     */
    public function existCode()
    {
        return $this->getCode();
    }

    /**
     * @return \Illuminate\Session\SessionManager|\Illuminate\Session\Store|mixed
     */
    public function existAccessToken()
    {
        return session($this->sessionKey);
    }

    /**
     * @return string
     */
    public function getAuthUrl()
    {
        $this->resetAccessToken();
        $query = http_build_query([
            'client_id' => $this->clientId,
            'redirect_uri' => $this->redirect,
            'response_type' => 'code'
        ]);

        return $this->baseUrl . "/oauth/authorize?" . $query;
    }


    /**
     * @return array|\Illuminate\Http\Request|string
     */
    public function getCode()
    {
        return request('code');
    }


    /**
     * @return \Illuminate\Session\SessionManager|\Illuminate\Session\Store|mixed
     */
    public function getAccessToken()
    {
        if (!$this->existAccessToken() && $this->existCode()) {
            $response = $this->http->post($this->baseUrl . '/oauth/token', [
                'form_params' => [
                    'grant_type' => 'authorization_code',
                    'client_id' => $this->clientId,
                    'client_secret' => $this->secret,
                    'redirect_uri' => $this->redirect,
                    "scope" => $this->scope,
                    'code' => $this->getCode(),
                ],
            ]);

            session([$this->sessionKey => json_decode((string)$response->getBody(), true)['access_token']]);
        };

        return session($this->sessionKey);
    }

    /**
     * @return \Illuminate\Session\SessionManager|\Illuminate\Session\Store|mixed
     */
    public function resetAccessToken()
    {
        session()->forget($this->sessionKey);
        return $this->getAccessToken();
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        if (!$this->user) {
            $response = $this->http->get($this->baseUrl . '/api/user', $this->getOptions());
            $this->user = json_decode((string)$response->getBody());
        }

        return $this->user;
    }

    protected function getOptions(){
        return [
            'headers' => [
                'Authorization' => 'Bearer ' . session($this->sessionKey)
            ]
        ];
    }
}