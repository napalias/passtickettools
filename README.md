# Pass Ticket

#### Before need installed laravel auth
```bash
php artisan make:auth
```


#### Add to composer
```json
"require": {
    "napalias/passticket" : "master@dev"
}

"repositories" : [
    {
        "type" : "vcs",
        "url" : "https://github.com/napalias/passticket.git",
        "reference": "master@dev"
    }
]
```

#### Composer update
 ```bash
composer update
```

#### Create Migration
```php
/**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->unsignedInteger('passport_user_id')->before('created_at')->nullable();
            $table->string('password')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('passport_user_id');
            $table->string('password')->notNullable()->change();
        });
    }
```

#### Run migrations
```bash
php artisan migrate
```

#### Add to 'config/services.php'
```php
'passticket' => [
        'id' => env('PASSTICKET_ID'),
        'secret' => env('PASSTICKET_SECRET'),
        'server' => env('PASSTICKET_SERVER'),
        'redirect' => env('PASSTICKET_REDIRECT'),
        'scope' => env('PASSTICKET_SCOPE', ""),
        'login_redirect' => env('PASSTICKET_LOGIN_REDIRECT', "")
    ]
```

#### Add to .env
```
PASSTICKET_ID=
PASSTICKET_SECRET=
PASSTICKET_SERVER=
PASSTICKET_REDIRECT=
PASSTICKET_LOGIN_REDIRECT=
```

#### Revmove Laravel auth routes
```php
Auth::routes();
```

#### And replacee with routes
```php
Route::get('login', '\PassTicket\Http\Controllers\AuthController@login')->name('login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');
```

